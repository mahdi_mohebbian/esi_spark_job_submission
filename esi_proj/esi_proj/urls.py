"""esi_proj URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
from esi_app import views
from . import settings
from django.conf.urls.static import static


urlpatterns = [
    url(r'^admin/', admin.site.urls),
    
    url(r'^api/job_item/(?P<pk>[0-9]+)/', view= views.Job_item.as_view()),
    url(r'^api/job', view= views.Jobs.as_view()),
    url(r'^api/pyjob_item/(?P<pk>[0-9]+)/', view= views.Pyjob_item.as_view()),
    url(r'^api/pyjob', view= views.Pyjob.as_view()),
    url(r'^api/trace', view= views.TraceJobs.as_view()),
    url(r'^api/pytrace', view= views.TracePythonJobs.as_view()),
    url(r'^api/login', view= views.LoginAPiView.as_view()),
    url(r'^api/uploaditem/(?P<pk>[0-9]+)/', view=views.Upload_item.as_view()),
    url(r'^api/upload', view= views.Upload.as_view()),
    url(r'^api/register', view= views.Register.as_view()),

    

]

urlpatterns += static(settings.MEDIA_URL, document_root = settings.MEDIA_ROOT)


