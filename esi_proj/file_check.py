import os
import django
import datetime

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "esi_proj.settings")
django.setup()

import psutil
from esi_app import models
from django.db import models as model_class

from apscheduler.schedulers.blocking import BlockingScheduler
import requests
import json





# # this function is designed to send notifications to specific users
# def SendNotification_to_userIDs(id_list, authorization_key, content, heading, app_id):

#     header = {"Content-Type": "application/json; charset=utf-8",
#               "Authorization": "Basic "+authorization_key}

#     payload = {"app_id": app_id,
#                "include_player_ids": id_list,
#                "contents": {"en": content},
#                "headings": {"en": heading}}

#     req = requests.post("https://onesignal.com/api/v1/notifications", headers=header, data=json.dumps(payload))

#     print(req.status_code, req.reason)


# # this function is designed to send notifications to all users
# def SendNotificationToAll(authorization_key, content, heading, app_id):
#     header = {"Content-Type": "application/json; charset=utf-8",
#               "Authorization": "Basic "+authorization_key}

#     payload = {"app_id": app_id,
#                "included_segments": ["All"],
#                "contents": {"en": content},
#                "headings": {"en": heading}}

#     req = requests.post("https://onesignal.com/api/v1/notifications", headers=header, data=json.dumps(payload))

#     print(req.status_code, req.reason)
#     print("sent to all")



def CheckResults():
    print("check-")
    jobs = models.PythonJobs.objects.filter(status_ready=False,)
    for job in jobs :
        if job.pid != "done" :
            print("item pid : {} is still active:".format(job.pid))
            t = job.num_of_check
            t+=1
            job.num_of_check = t
            job.save()
            if t > 6 :
                print("item pid : {} is terminated:".format(job.pid))
                os.system("echo {} >> {}".format("TIME OUT ERROR ,CHECK YOUR CODE !",str(job.file.path)+"-result_log-.txt"))
                p = psutil.Process(int(job.pid))
                p.terminate() 
                job.pid = "done"
                job.result = str(job.file.path)+"-result_log-.txt"
                job.status_ready = True
                job.num_of_check = 0
                job.save()




# use the job scheduler to start them an run each checking code on a specified time
scheduler = BlockingScheduler()
scheduler.add_job(CheckResults, 'interval', seconds=10)
scheduler.start()

