from django.shortcuts import render
import os
import requests
from . import models
from rest_framework.response import Response
from rest_framework import status
from rest_framework.views import APIView
from rest_framework.authtoken.models import Token
from rest_framework import status
from django.contrib.auth import hashers
from rest_framework import permissions
import subprocess
from . import serializers
from esi_proj import settings
# Create your views here.

class Jobs(APIView):
    def get(self,request):
        order = request.query_params.get('order', None)
 
        if order == None :
            jobs = models.Job.objects.filter(user=request.user).order_by('-date', '-time')
        else :
            if order == "date":
                jobs = models.Job.objects.filter(user=request.user).order_by('-date', '-time')
            else :
                jobs = models.Job.objects.filter(user=request.user).order_by('-'+order)
        
        seri = serializers.JobsSerializers_general(jobs,many=True)
        return Response(seri.data,status=status.HTTP_200_OK)
        

    def post(self,request):
        
        
        if len(models.Job.objects.filter(app_name=request.data["app_name"]))>0:
            return Response({"message":"change appname"},status=status.HTTP_400_BAD_REQUEST)
        
        job = models.Job(category="spark",file=request.data["file"],user=request.user,app_name=request.data["app_name"])

        job.save()
        
        if job.category == "hadoop":
            cmd = "/usr/local/hadoop/bin/hadoop jar {} grep ~/input ~/grep_example2 'allowed[.]*'".format(job.file.path)
            os.system(cmd)
            return Response({"":""})


        elif job.category == "spark":
            file_exten = job.file.__str__().split(".")[-1]
            if file_exten == "py":

                
                cmdList = ["/home/mahdi/Downloads/spark-2.4.1-bin-hadoop2.7/bin/spark-submit","--master","spark://mahdi-HP-EliteBook-840-G5:7077",job.file.path] #"--class", "com.spark.myapp", "./myapp.jar"]

                if "arg" in request.data:
                    file_arg = request.data["arg"]
                    job.arg = file_arg
                    job.save()
                    cmdList.append(settings.MEDIA_ROOT+"/jar_files/"+job.arg)
                    print(cmdList)
                    
                p = subprocess.Popen(cmdList,stdout=subprocess.PIPE,stderr=subprocess.PIPE)
                stdout, stderr = p.communicate() 
                stderr=stderr.splitlines()
                stdout=stdout.splitlines()

                os.system("echo {} >> {}".format(stderr,str(job.file.path)+"-log-error.txt"))
                job.err_file = str(job.file)+"-log-error.txt"

                os.system("echo {} >> {}".format(stdout,str(job.file.path)+"-log-output.txt"))
                job.out_file = str(job.file)+"-log-output.txt"
                job.status_ready = True
                job.save()
                
                print("STDERR : {}".format(stderr))
                print("STDOUT : {}".format(stdout))
            
            return Response({"message":"success"})


        def delete(self, request):
            models.Job.objects.get(pk=request.data['pk']).delete()
            return Response({"message":"item deleted"},status=status.HTTP_200_OK)

class Job_item(APIView):
    def delete(self, request, pk, format=None):
        snippet = models.Job.objects.get(id=int(pk))
        snippet.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


class TraceJobs(APIView):
    def get(self,request):
        pass
    
    def post(self,request):
        app_name = request.data["app_name"]
        r = requests.get("http://localhost:18080/api/v1/applications")
        data = r.json()
        result = list()
        


        if len(data)>0:
                
            for item in data:
                if item["name"]==app_name :
                    result.append(item)
                    
            return Response({"message":result},status=status.HTTP_200_OK)

        else :
            return Response({"message":"no job available"},status=status.HTTP_204_NO_CONTENT)


    
class Pyjob(APIView):
    def get(self,request):

        pyjobs = models.PythonJobs.objects.filter(user=request.user).order_by('-date','-time')
        serial = serializers.PythonSerializer(pyjobs,many=True)
        return Response(serial.data,status=status.HTTP_200_OK)
        
    
    def post(self,request):
        code = request.data["code"]
        name = request.data["name"]
        if request.data["rep"] != "":
            repo = request.data["rep"]
            py = models.PythonJobs(file=code,repo=repo,name=name,user=request.user)
        else :
            
            py = models.PythonJobs(file=code,user=request.user,name=name)
        py.save()
        
        if request.data["rep"] != "":
            repo_list = py.repo.split(",")
            correct_repo_list = list()
            for item in repo_list:
                correct_repo_list.append(settings.MEDIA_ROOT+"/jar_files/"+item)
            print(correct_repo_list)
            pyapp_cmd = ["python",py.file.path]
            
            for item in correct_repo_list:
                pyapp_cmd .append(item)
            
            process = subprocess.Popen(pyapp_cmd,stdout=subprocess.PIPE,stderr=subprocess.PIPE)
        else :
            process = subprocess.Popen(["python",py.file.path],stdout=subprocess.PIPE,stderr=subprocess.PIPE)
        py.pid = process.pid
        py.save()
        # os.system("python {} >> {}".format(py.file.path,str(py.file.path)+"-result_log-.txt"))
        stdout,stderr = process.communicate()
        
        os.system("echo {} >> {}".format(str(stdout),str(py.file.path)+"-result_log-.txt"))
        print("+++++++++++++++++++++++++++")
        error = str(stderr).replace('"','\\')
        error = error.replace("'","\\")
        error = error.replace(" ","_")
        error = "'"+error+"'"
        print(error)
            
        print("+++++++++++++++++++++++++++")
        if len(stderr) > 0:
            os.system('./bash.sh {} {}'.format(error,str(py.file.path)+"-result_log-.txt"))
            py.result = str(py.file)+"-result_log-.txt"
            py.pid = "done"
            py.status_ready = True
            py.save()
            return Response({"msg":stderr})
        py.result = str(py.file)+"-result_log-.txt"
        py.pid = "done"
        py.status_ready = True
        py.save()

        
        return Response({"message":"success"})


class Pyjob_item(APIView):
    def delete(self, request, pk, format=None):
        snippet = models.PythonJobs.objects.get(id=int(pk))
        snippet.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

class TracePythonJobs(APIView):
    def get(self,request):
        pass

    def post(self,request):
        show_lvl = request.data["show_level"]
        
        if show_lvl == "1":# show only finished ones
            pyjobs = models.PythonJobs.objects.filter(status_ready=True,user=request.user)
            print(pyjobs)
            serial = serializers.PythonSerializer(pyjobs,many=True)
            return Response(serial.data,status=status.HTTP_200_OK)
        

        elif show_lvl == "2":# show only non finished
            pyjobs = models.PythonJobs.objects.filter(status_ready=False,user=request.user)
            serial = serializers.PythonSerializer(pyjobs,many=True)
            return Response(serial.data,status=status.HTTP_200_OK)


        else : # show all of them
            pyjobs = models.PythonJobs.objects.filter(user=request.user)
            serial = serializers.PythonSerializer(pyjobs,many=True)
            return Response(serial.data,status=status.HTTP_200_OK)


class LoginAPiView(APIView):
    permission_classes = (permissions.AllowAny,)
    def post(self,request):
        user_name = request.data['username']
        password = request.data['password']

        try :
            user = models.UserProfile.objects.get(email=user_name)
        except :
            user = None

        if user == None:
            return Response({'message':'wrong email'},status=status.HTTP_400_BAD_REQUEST)

        if hashers.check_password(password,user.password)==True:
            
            try:
                token = Token.objects.get(user=user)
            except:
                token=None

            if token == None:
                token = Token.objects.create(user=user)

            else:
                token.delete()
                token = Token.objects.create(user=user)



            return Response({'Token':token.__str__()},status=status.HTTP_200_OK)
        return Response({'message':'wrong password'},status=status.HTTP_400_BAD_REQUEST)


class Upload(APIView):

    def get(self,request):
        uploads = models.Upload.objects.filter(user=request.user).order_by('-date')
        serial = serializers.UploadsSerializer(uploads,many=True)

        return Response(serial.data,status=status.HTTP_200_OK)

    def post(self,request):
        user = request.user
        file = request.data["file"]
        desc = request.data["desc"]
        name = request.data["name"]
        upload = models.Upload(desc=desc,name=name,file=file,user=user)
        upload.save()
        print(upload.file)
        return Response({"message":"success"},status=status.HTTP_201_CREATED)

    
class Upload_item(APIView):

    def delete(self, request, pk, format=None):
        snippet = models.Upload.objects.get(id=int(pk))
        snippet.delete()
        return Response({"message":"item has been deleted"},status=status.HTTP_204_NO_CONTENT)


class Register(APIView):
    permission_classes = (permissions.AllowAny,)
    def get(self,request):
        pass

    def post(self,request):
        email = request.data["username"]
        password = request.data["password"]
        tell = request.data["tell"]
        name = request.data["name"]
        users = models.UserProfile.objects.filter(email=email)

        if len(users)>0 :
            return Response({"message":"email address is not available for you"},status=status.HTTP_406_NOT_ACCEPTABLE)
        hash_pass = hashers.make_password(password=password)
        user = models.UserProfile(name=name,email=email,phone_number=tell,password=hash_pass)
        user.save()

        return Response({"message":"registered succussfuly"},status=status.HTTP_201_CREATED)