# -*- coding: utf-8 -*-
# Generated by Django 1.11.6 on 2019-05-09 12:05
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('esi_app', '0018_auto_20190508_1731'),
    ]

    operations = [
        migrations.AddField(
            model_name='pythonjobs',
            name='time',
            field=models.TimeField(auto_now=True, null=True),
        ),
    ]
