# -*- coding: utf-8 -*-
# Generated by Django 1.11.6 on 2019-05-08 11:47
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('esi_app', '0015_auto_20190507_1733'),
    ]

    operations = [
        migrations.CreateModel(
            name='Upload',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(default='uploaded file', max_length=50)),
                ('desc', models.TextField(default='this is an uploaded file')),
                ('date', models.DateField(auto_now=True, null=True)),
                ('file', models.FileField(null=True, upload_to='py_files/')),
                ('user', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'ordering': ['-date'],
            },
        ),
        migrations.AlterModelOptions(
            name='job',
            options={'ordering': ['-time', '-date']},
        ),
    ]
