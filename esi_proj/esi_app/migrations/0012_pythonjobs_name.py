# -*- coding: utf-8 -*-
# Generated by Django 1.11.6 on 2019-04-25 13:27
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('esi_app', '0011_auto_20190418_1422'),
    ]

    operations = [
        migrations.AddField(
            model_name='pythonjobs',
            name='name',
            field=models.CharField(default='python_file', max_length=50),
        ),
    ]
