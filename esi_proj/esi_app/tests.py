

from rest_framework.test import APIRequestFactory
from rest_framework.test import force_authenticate
from . import views
from . import models

factory = APIRequestFactory()
user = models.UserProfile.objects.get(email='mahdi.mohebbian@yahoo.com')
view = views.Jobs.as_view()

# Make an authenticated request to the view...
request = factory.get('api/job')
force_authenticate(request, user=user)
response = view(request)
print(response)