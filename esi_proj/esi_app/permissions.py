from rest_framework import permissions

class AllowOptionsAuthentication(permissions.BasePermission):
    def has_permission(self, request,view):
        if request.method == 'OPTIONS':
            return True
        return request.user and request.user.is_authenticated