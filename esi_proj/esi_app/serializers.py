from rest_framework import serializers
from . import models

class UserSerializers(serializers.ModelSerializer):
    class Meta:
        model = models.UserProfile
        fields = '__all__'

class JobsSerializers(serializers.ModelSerializer):
    class Meta:
        model = models.Job
        fields = ('out_file','err_file')

class JobsSerializers_general(serializers.ModelSerializer):
    class Meta:
        model = models.Job
        fields = ('__all__')

class PythonSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.PythonJobs
        fields = ('__all__')

class UploadsSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Upload
        fields = ('__all__')