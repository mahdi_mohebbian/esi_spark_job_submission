from django.db import models

import os

import django.utils.timezone as django_date
from django.contrib.auth.base_user import BaseUserManager, AbstractBaseUser
from django.contrib.auth.models import PermissionsMixin
from django.db import models
from django.dispatch import receiver
import datetime


class UserProfileManager(BaseUserManager):

    def create_user(self,email,name,phone_number,password=None):

        email = self.normalize_email(email)
        user = self.model(email=email,name=name,phone_number=phone_number,password=password)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, email, name, phone_number, password=None):
        user = self.create_user(email=email,name=name,phone_number=phone_number,password=password)
        user.is_staff = True
        user.is_superuser = True
        user.save(using=self._db)




class UserProfile(AbstractBaseUser,PermissionsMixin):
    name = models.CharField(max_length=100)
    phone_number = models.BigIntegerField(unique=True)
    email = models.EmailField(unique=True)
    created_on = models.DateField(auto_now=True)
    is_active = models.BooleanField(default=True)
    is_staff = models.BooleanField(default=False)
    objects = UserProfileManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['phone_number', 'name']

    def get_short_name(self):
        return self.name

    def get_full_name(self):
        return self.name

    def __str__(self):
        return self.id.__str__()+" : "+self.name


class Job(models.Model):
    category = models.CharField(default="hadoop",max_length=20,choices=[("spark","Spark Job"),("hadoop","hadoop job")])
    # date = models.DateField(auto_now=True,null=True)
    date = models.DateField(auto_now_add=True , null=True)
    time = models.TimeField(auto_now_add=True,null=True)
    app_name = models.CharField(default="WordCount",max_length=50)
    user = models.ForeignKey(UserProfile,null=True,on_delete=models.CASCADE)
    file = models.FileField(null=True,upload_to='jar_files/')
    arg = models.CharField(null=True,max_length=100)
    out_file = models.FileField(null=True,upload_to='jar_files/')
    err_file = models.FileField(null=True,upload_to='jar_files/')
    status_ready = models.BooleanField(default=False)
    def __str__(self):
        return self.category +" - "+self.app_name+" :"+ str(self.status_ready)
    class Meta:
        ordering = ['-time','-date']


class PythonJobs(models.Model):
    name = models.CharField(null=False,max_length=50,default="python_file")
    date = models.DateField(auto_now=True,null=True)
    time = models.TimeField(auto_now=True,null=True)
    file = models.FileField(null=True,upload_to='py_files/')
    repo = models.CharField(null=True,max_length=100)
    result = models.FileField(null=True,upload_to='py_files/')
    user = models.ForeignKey(UserProfile,null=True,on_delete=models.CASCADE)
    status_ready = models.BooleanField(default=False)
    pid = models.CharField(default="999999",max_length=10)
    collected = models.BooleanField(default=False)
    num_of_check = models.IntegerField(default=0)
    def __str__(self):
        return str(self.file) +" - is ready : " +str(self.status_ready)

class Upload(models.Model):
    name = models.CharField(null=False,max_length=50,default="uploaded file")
    desc = models.TextField(null=False,default="this is an uploaded file")
    date = models.DateField(auto_now=True,null=True)
    file = models.FileField(null=True,upload_to='jar_files/')
    user = models.ForeignKey(UserProfile,null=True,on_delete=models.CASCADE)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ['-date']
